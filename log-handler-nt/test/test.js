//const expect = require('assert').expect;
//const should = require('chai').should();

const lib = require('../index.js');


describe("Config", () =>
{
  it("Should not fail", () =>
  {
    lib.config(require('../configWinston.js'));
  });
});

describe("Log production 1", () =>
{
  it("Should write in file", () =>
  {
    process.env.NODE_ENV = "production";
    lib.log('info', "Test.", "Testing process");
  });
});


describe("Log debug Dev 1", () =>
{
  it("Should not write on the console", () =>
  {
    process.env.NODE_ENV = "dev";
    lib.log('debug', "Test.", "[Testing process]");
  });
});


describe("Log error Dev 2", () =>
{

  it("Should write red on the console", () =>
  {
    process.env.NODE_ENV = "dev";
    lib.log('error', "Test.", "[Testing process]");
  });
});



describe("Log debug Debug 1", () =>
{
  it("Should write on the console", () =>
  {
    process.env.NODE_ENV = "debug";
    lib.log('debug', "Test.", "[Testing process]");
  });
});





